import { Path, Vertex, Edge } from '../types.ts'


type langVertexMap = {[lang: string]: Vertex}

// A recursive function to print all paths from 'u' to 'd'. 
// visited[] keeps track of vertices in current path. 
// path[] stores actual vertices and path_index is current 
// index in path[]
// based on Depth First Traversal

function pathTraversal(langVertexMap: langVertexMap, u: string, d: string, visited: {[id: string]: boolean}, curPath: Edge[], out: Path[]) {
    // Mark the current node as visited and store in path 
    visited[u] = true

    // If current vertex is same as destination, then print / store
    // current path[] 
    if(u == d) {
        if(curPath.length) {
            const p: Path = {
                fromLang: curPath[0].fromLang,
                toLang: d,
                edges: curPath.slice(0), // clone
                usedInterpreters: {}
            }
            for(const edge of curPath) {
                p.usedInterpreters[edge.viaInterpreter] = edge.fromLang+':'+edge.toLang
            }
            // console.log(p)
            out.push(p)
        }
    } else {
        // If current vertex is not destination and not via an already used interpreter
        //Recur for all the vertices adjacent to this vertex 
        const includedInterpreters = curPath.map(e => e.viaInterpreter)
        for(const edge of langVertexMap[u].outboundEdges) {
            if(!visited[edge.toLang] && !includedInterpreters.includes(edge.viaInterpreter)) {
                curPath.push(edge)
                pathTraversal(langVertexMap, edge.toLang, d, visited, curPath, out)
                curPath.pop()
            }
        }
    }
    visited[u] = false
}


export default function findPaths(from: string, to: string, vertices: Vertex[]) {
    if(from == to) {
      return [
        {
          fromLang: from,
          toLang: to,
          edges: [],
          usedInterpreters: {}
        }
      ]
    }
    const paths: Path[] = []
    const visited: {[id: string]: boolean} = {}
    const path: Edge[] = []
    // deno-lint-ignore no-explicit-any
    const langVertexMap = vertices.reduce((o: any,v) => { o[v.lang] = v; return o }, {})

    pathTraversal(langVertexMap, from, to, visited, path, paths)
    return paths
}