export type LanguagePriorities = {
  [lang: string]: number
  default: number
}

export interface LanguageTuple {
    accepts?: string
    provides?: string
    priority?: number
}

export enum ElementType {
    /**
     * @deprecated use Floor instead
     */
    Stage = 'stage',

    Floor = 'stage',
    Interpreter = 'int',
    Transmitter = 'tx',
    Monitor = 'mon',
    LineOut = 'out'
}
export interface Element {
    id: string
    type: ElementType
    langs: LanguageTuple[]
    label?: string
}

export interface Edge {
    fromLang: string
    toLang: string
    viaInterpreter: string
    // viaLangIndex: number
    // priority: number
}
export interface Vertex {
    lang: string,
    outboundEdges: Edge[],
    inboundEdges: Edge[]
}
export interface Path {
    fromLang: string,
    toLang: string
    edges: Edge[],
    usedInterpreters: {[id: string]: string}
}

export interface ReducedElement {
    id: string
    accepts?: string
    provides?: string
}
